const express = require("express");
const dotenv = require("dotenv");
const path = require("path");
const app = express();
const http = require("http").Server(app);
const io = require("socket.io")(http);
const { log } = require("console");
const publicpath = path.join(__dirname, "public");
const cors = require("cors");
const connectdatabase = require("./config/db");
let socketids = [];
const usermodel = require("./models/usermodel");

dotenv.config({ path: "./config/config.env" });

connectdatabase();

app.set("views", path.join(__dirname, "/views"));
app.set("view engine", "ejs");
app.use(express.json());
app.use(express.urlencoded());

app.post("/auth/register", async (req, res, next) => {
    console.log("req body : ", req.body);
    const { email, password, name } = req.body;
    try {
        const data = await usermodel.create({ email: email, name: name, password: password });
        res.status(200).json({ success: true, data: data });
    } catch (err) {
        res.status(400).json({ success: false, error: err.message });
    }
});

app.get("/auth/login", (req, res) => {
    res.render("login.ejs");
});
app.get("/", async (req, res, next) => {
    res.send(`
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
		<div class="container">
        <h1 class="text-center mt-3 mb-3">Submit Form Data in Node.js</h1>
        <div class="card">
        <div class="card-header">Sample Form</div>
        <div class="card-body">
        <form method="POST" action="/">
        <div class="mb-3">
            <label>Email</label>
            <input type="text" name="email" id="email" class="form-control" />
            </div>
            <div class="mb-3">
            <label>password</label>
            <input type="text" name="password" id="password" class="form-control" />
            </div>
            <div class="mb-3">
            <input type="submit" id="submit_button" class="btn btn-primary"/>
            </div>
            </form>
            </div>
			</div>
		</div>
	`);
});
app.post("/", async (req, res, next) => {
    const { email, password } = req.body;
    if (!email || !password) {
        res.redirect("/auth/login");
    }
    // const allusers = await usermodel.find({},{_id:0,name:1})
    const data = await usermodel.findOne({ email: email, password: password });
    if (!data) {
        res.redirect("/auth/login");
    }
    res.render("index.ejs", { data: data, name: data.name, rooms: data.rooms });
});
app.post("/addgroups/:name/:group", async (req, res) => {
    try {
        const data = await usermodel.findOneAndUpdate({ name: req.params.name }, { $addToSet: { rooms: req.params.group } });
        console.log(data);
        res.status(200).json({ data: data });
    } catch (err) {
        res.status(400).json({ err: err.message });
    }
});

const logger = (req, res, next) => {
    console.log(`${req.method} ${req.protocol}://${req.hostname}:${process.env.PORT}${req.originalUrl}`);
    next();
};
app.use(logger);

let rooms = [];

io.on("connection", (socket) => {
    console.log("user is connected with", socket.id);
    if (socket.handshake.auth.name) {
        socketids.push({ name: socket.handshake.auth.name, socketid: socket.id });
    }
    io.emit("allsockets", socketids);
    socket.on("disconnect", () => {
        console.log(`Socket disconnected: ${socket.id}`);
        const index = findindex(socketids, socket.id);
        console.log("answer is ", index);
        // const index = connectedSocketIds.indexOf(socket.id);
        if (index !== -1) {
            socketids.splice(index, 1);
        }
    });
    io.emit("refreshproblem", socketids);
    try {
        console.log("handshake headers", socketids);
        const arr = socket.handshake.auth.user.split(",");
        arr.forEach((element) => {
            socket.join(element);
        });
    } catch (err) {
        console.log("errrrrrrrrrr::", err.message);
    }
    socket.emit("groups", rooms);
    socket.on(
        "custom-message",
        async function (msg) {
            console.log("here", socket.rooms, msg.name);

            if (!msg.room) {
                socket.broadcast.emit("receive-message", msg.msg);
            } else {
                if (msg.room.split(":").length > 1) {
                    console.log(msg.room);
                    msg.room = msg.room.split(":")[1];

                    socket.to(msg.room).emit("receive-message", msg.msg, msg.room, msg.name);
                } else {
                    socket.to(msg.room).emit("receive-message", msg.msg, msg.room, msg.name);
                }
            }
        },
        { user: "1" }
    );

    socket.on("joinroom", async (room, name) => {
        try {
            if (room.length >= 1) {
                const data1 = await usermodel.findOne({ name: name });
                const data = await usermodel.findByIdAndUpdate(data1._id, { $addToSet: { rooms: room } });
                console.log(data);
            }
        } catch (err) {
            throw err;
        }
    });
});
function findindex(arr, key) {
    for (let i = 0; i < arr.length; i++) {
        console.log(arr[i].socketid, key);
        if (arr[i].socketid === key) {
            return i;
        }
    }
}
http.listen(3000, () => {
    console.log(`Socket.IO server running at http://localhost:3000`);
});
