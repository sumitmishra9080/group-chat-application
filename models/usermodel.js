const mongoose = require('mongoose')

const userschema = mongoose.Schema({
    name:{
        type:String,
        unique:true,
        maxlength:50,
    },
    password:{
        type:String,
        required:true,
        minlenth:6,
        
    },
    email:{
        type:String,
        unique:true,
        maxlength:100,
        match:[/^[\w.-]+@[a-zA-Z\d.-]+\.[a-zA-Z]{2,}$/,"wrong mail"]
    },
    rooms:{
        type:[String],
        default:"all"
    }
})




module.exports = mongoose.model('User',userschema)