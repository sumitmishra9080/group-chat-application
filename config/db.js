const mongoose = require('mongoose');

const connectDB = async ()=>{
    console.log(process.env.MONGODB_URI);
    const conn = await mongoose.connect(process.env.MONGODB_URI,{
        useNewUrlParser: true,
        useUnifiedTopology: true,
    });
    console.log(`MONGODB connected : ${conn.connection.port}`);
}

module.exports = connectDB;